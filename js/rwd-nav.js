/* Scripts for the tables test page
   Author: Maggie Wachs, www.filamentgroup.com
   Date: November 2011
   Dependencies: jQuery, jQuery UI widget factory
*/



jQuery(function($){

   $('.primary-nav')
      // test the menu to see if all items fit horizontally
      .bind('testfit', function(){
            var nav = $(this),
                items = nav.find('a');

            $('body').removeClass('show-menu');

            // when the nav wraps under the logo, or when options are stacked, display the nav as a menu
            if ( (nav.offset().top > nav.prev().offset().top) || ($(items[items.length-1]).offset().top > $(items[0]).offset().top) ) {

               // add a class for scoping menu styles
               $('body').addClass('show-menu');

            };
         })

      // toggle the menu items' visiblity
      .find('h3')
         .bind('click focus', function(){
            $(this).parent().toggleClass('expanded')
         });

   // ...and update the nav on window events
   $(window).bind('load resize orientationchange', function(){
      $('.primary-nav').trigger('testfit');
   });

});
